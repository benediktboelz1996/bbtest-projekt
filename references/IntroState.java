package gamestate;

import gamestate.map.lule.LuleMap;
import handler.Keys;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import main.GamePanel;

/**
 * Das Intro zum Spiel wird normal nur bei einem neuen Spiel angezeigt
 */
public class IntroState extends GameState {

	// Hintergrund
	private BufferedImage bg;

	// Text des Intro
	private String text;
	private String[] textWords;
	private int currentChar = 0, currentWord = 0, lastCharWord = 0;
	private int x, y;

	protected IntroState(String name) {
		super(name);
		// TODO Text sinnvoll befüllen
		text = "Willkommen, deine Reise wird jeden Augenblick beginnen. "
				+ "Nimm dir doch bitte kurz Zeit und entspanne dich. "
				+ "In diesem Königreich kannst nur du die Prinzessin retten... höchstwahrscheinlich. "
				+ "Nur keinen Druck und viel Spaß. ";
		textWords = text.split(" ");
		// TODO Sound laden und abspielen
	}

	@Override
	public void onEnter() {
		currentChar = 0;
		currentWord = 0;
		lastCharWord = 0;
		x = 10;
		y = 20;
	}

	@Override
	public void update() {
		handleInput();
		if (currentChar < text.length()) {
			currentChar++;
		}
	}

	@Override
	public void draw(Graphics2D g) {
		// Hintergrund nur zeichnen bei erstem Aufruf
		if (currentChar == 0) {
			if (bg != null) {
				// TODO zeichne Hintergrund
			} else {
				g.setColor(Color.BLACK);
				g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
			}
		}

		// Text zeichnen
		// TODO sinnvolle Color nach Hintergrund
		g.setColor(Color.WHITE);
		g.setFont(new Font("Arial", Font.PLAIN, 14));
		FontMetrics fm = g.getFontMetrics();

		if (textWords.length > currentWord) {
			if (textWords[currentWord].length() < currentChar - lastCharWord) {
				currentWord++;
				lastCharWord = currentChar;
				// neue Zeile
				if (currentWord < textWords.length
						&& x + fm.stringWidth(textWords[currentWord]) > GamePanel.WIDTH - 10) {
					x = 10;
					y += 20;
				}
			}
		}

		// Zeichen zeichnen
		if (currentChar < text.length()) {
			g.drawString(text.charAt(currentChar) + "", x, y);
			x += fm.charWidth(text.charAt(currentChar));
		}
	}

	@Override
	public void onExit() {
		// Noch nichts
	}

	@Override
	protected void handleInput() {
		if (Keys.isPressed(Keys.BUTTON1)) {
			if (currentChar < text.length()) {
				// TODO Text komplett anzeigen
				currentChar = text.length();
			} else {
				// TODO weiter
				LuleMap lm = new LuleMap("lule");
				GameStateManager.getInstance().addState(lm);
				GameStateManager.getInstance().pushState(lm.getStatename());
			}
		} else if (Keys.isPressed(Keys.BUTTON2)) {
			if (currentChar < text.length()) {
				// TODO möglicherweise Speed up
			} else {
				// TODO weiter
			}
		}
	}

}
