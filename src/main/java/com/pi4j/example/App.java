package com.pi4j.example;

import com.pi4j.Pi4J;
import com.pi4j.context.Context;
import com.pi4j.example.hardware.ClientHardware;
import com.pi4j.example.hardware.ClientHardwareImpl;
import com.pi4j.example.logic.StatefulGameStateHandler;
import com.pi4j.util.Console;
import de.shgruppe.tischkicker.miniclient.MockTischkickerService;
import de.shgruppe.tischkicker.miniclient.TischKickerServiceImpl;
import de.shgruppe.tischkicker.miniclient.TischkickerService;

import java.io.IOException;
import java.util.Arrays;

public class App {
    static boolean isDisplayOn = true;

    static String textOnButtonOneClick = "";

    static String text = "";
    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("Hello world!");


        boolean remote = Boolean.parseBoolean(System.getProperty("remote"));

        final var console = new Console();

        Context pi4j = getPi4jContext(remote);

        PrintInfo.printLoadedPlatforms(console, pi4j);
        PrintInfo.printDefaultPlatform(console, pi4j);
        PrintInfo.printProviders(console, pi4j);
        console.println("remote="+remote);

        int[] buttonAddresses = getButtonAddresses();

        ClientHardware hardware = new ClientHardwareImpl(pi4j, buttonAddresses);
        hardware.getDisplay().setText("FI-AE-22\nintialisiere...");
        hardware.getDisplay().render();

        var service = getService();

        var handler = new StatefulGameStateHandler(hardware, service);
        handler.init();

        while(true){
            Thread.sleep(50);



        }

    }



    private static Context getPi4jContext(boolean remote) {
        return Pi4J.newContextBuilder()
                .property("remote", "" + remote)
                .autoDetectPlatforms()
                .autoDetectProviders()
                .build();
    }

    private static int[] getButtonAddresses() {
        String addressesValue = System.getProperty("button-addresses");
        String[] addresses = addressesValue.split(",");
        System.out.println("Parsed Addresses="+ Arrays.toString(addresses));

        int[] addressesAsInt = new int[6];
        for(int i=0; i< 6; i++){
            String address = addresses[i];
            addressesAsInt[i] = Integer.parseInt(address);
        }
        return addressesAsInt;
    }

    private static TischkickerService getService() {
        return new TischKickerServiceImpl();
    }

    void old(){
        /**
         textOnButtonOneClick = System.getProperty("button-one-text", "");

         hardware.addOnButtonClick(1, () -> {
         text = textOnButtonOneClick;
         hardware.getDisplay().setText(text);
         hardware.getDisplay().render();
         console.println(text);
         });

         hardware.addOnButtonClick(2, () -> {
         text = "zwo\n";
         hardware.getDisplay().setText(text);
         hardware.getDisplay().render();
         console.println(text);
         });

         hardware.addOnDoubleClick(2, () -> {
         text = "zwo\nund nochmal zwei😅";
         hardware.getDisplay().setText(text);
         hardware.getDisplay().render();
         console.println(text);
         });

         hardware.addOnButtonClick(3, () -> {
         text = "drei\n";
         hardware.getDisplay().setText(text);
         hardware.getDisplay().render();
         console.println(text);
         });

         hardware.addOnButtonClick(4, () -> {
         text = "quatre\n";
         hardware.getDisplay().setText(text);
         hardware.getDisplay().render();
         console.println(text);
         });

         hardware.addOnButtonClick(5, () -> {
         text = "finf\n";
         hardware.getDisplay().setText(text);
         hardware.getDisplay().render();
         console.println(text);
         });

         hardware.addOnButtonClick(6, () -> {
         text = "sechs\n";
         hardware.getDisplay().setText(text);
         hardware.getDisplay().render();
         console.println(text);
         });
         */
    }
}