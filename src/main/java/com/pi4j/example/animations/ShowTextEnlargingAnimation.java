package com.pi4j.example.animations;

import com.pi4j.example.driver.Sh1106Display;
import com.pi4j.example.driver.TextRenderOptions;

import java.util.function.Consumer;

public class ShowTextEnlargingAnimation implements Consumer<Sh1106Display> {
    String text;
    public ShowTextEnlargingAnimation(String text) {
        this.text = text;
    }

    @Override
    public void accept(Sh1106Display display) {
        TextRenderOptions options = TextRenderOptions.builder().build();
        options.setFontSize(14);

        display.setText(text, options);
        display.render();

        sleep(1000L);
        options.setFontSize(20);
        display.setText(text, options);
        display.render();

        sleep(2000L);
        options.setFontSize(40);

        display.setText(text, options);
        display.render();

        sleep(3000L);
    }

    void sleep(long millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
