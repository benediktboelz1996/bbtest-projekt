package com.pi4j.example.driver;

import java.util.function.Consumer;

public interface AnimationAwareSh1106Dispaly extends Sh1106Display {

    /**
     * Plays an animation. After the animation ends, the display will be reset to the original screen data.
     * @param animation the animation that can write to the display.
     */
    void playAnimation(Consumer<Sh1106Display> animation);
}
