package com.pi4j.example.driver;

import com.pi4j.io.i2c.I2C;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.function.Consumer;

@Slf4j
public class AnimationAwareSh1106DisplayImpl implements AnimationAwareSh1106Dispaly{
    private final Sh1106DisplayImpl delegate;

    private volatile boolean isAnimationRunning;


    public AnimationAwareSh1106DisplayImpl(Sh1106DisplayImpl delegate){
        this.delegate = delegate;
    }

    @Override
    public void playAnimation(Consumer<Sh1106Display> animation) {
        if(isAnimationRunning){
            //TODO throw Exception?
            log.warn("cannot run multiple animations.");
            return;
        }

        Sh1106Display displayForAnimation = delegate.getCopyOfThis();
        new Thread(() -> {
            isAnimationRunning = true;
            animation.accept(displayForAnimation);
            isAnimationRunning = false;
            // render previous or current(if methods were invoked while playing the animation) screen data.
            delegate.render();
        }).start();

    }

    @Override
    public void render() {
        if(isAnimationRunning){
            // do nothing to prevent that multiple
            return;
        }
        delegate.render();
    }

    @Override
    public void setPixel(int x, int y, boolean on) throws IllegalArgumentException {
        delegate.setPixel(x,y,on);
    }

    @Override
    public int getLcdWidth() {
        return delegate.getLcdWidth();
    }

    @Override
    public int getLcdHeight() {
        return delegate.getLcdHeight();
    }

    @Override
    public void clearPixels() {
        delegate.clearPixels();
    }

    @Override
    public void setText(String text) {
        delegate.setText(text);
    }

    @Override
    public void setText(String text, TextRenderOptions options) {
        delegate.setText(text, options);
    }

    @Override
    public void close() throws IOException {
        if(isAnimationRunning){
            //TODO retry later?
            return;
        }
        delegate.close();
    }
}
