package com.pi4j.example.driver;

import java.awt.image.BufferedImage;

public class BitmapHelper {
    private static final int GRADIENT_CUTOFF = 170; // Tune for gradient picker on grayscale images.

    BitmapHelper() {
    }

    /**
     * Converts a bitmap image to LCD screen data and sets it on the given screen at the specified
     * offset.
     *
     * @param mScreen   The OLED screen to write the bitmap data to.
     * @param xOffset   The horizontal offset to draw the image at.
     * @param yOffset   The vertical offset to draw the image at.
     * @param image       The bitmap image that you want to convert to screen data.
     */
    void setBmpData(Sh1106DisplayImpl mScreen, int xOffset, int yOffset, BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        int bmpByteSize = (int) Math.ceil((double) (width * (Math.max((height / 8), 1))));
        //offset first line
        int offset = 1;

        // Each byte stored in memory represents 8 vertical pixels.  As such, you must fill the
        // memory with pixel data moving vertically top-down through the image and scrolling
        // across, while appending the vertical pixel data by series of 8.
        for (int y = 0; y < height; y += 8) {
            for (int x = 0; x < width; x++) {
                int page = y / 8;
                int bytePos = offset + x + (page * width);

                for (int k = 0; k < 8; k++) {
                    if ((k + y < height) && (bytePos < bmpByteSize)) {
                        int pixel = image.getRGB(x, y + k);

                        // Look at Alpha channel instead
                        if ((pixel & 0xFF) > GRADIENT_CUTOFF) {
                            mScreen.setPixel(x + xOffset, y + yOffset + k, true);
                        }
                        else {
                            mScreen.setPixel(x + xOffset, y + yOffset + k, false);
                        }
                    }
                }
            }
        }
    }

    /**
     * Converts a bitmap image to LCD screen data and returns the screen data as bytes.
     *
     * @param buffer The screen's data buffer.
     * @param offset The byte offset to start writing screen bitmap data at.
     * @param image The image that you want to convert to screen data.
     */
    public void bmpToBytes(byte[][] buffer, int offset, BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();

        // Each byte stored in memory represents 8 vertical pixels.  As such, you must fill the
        // memory with pixel data moving vertically top-down through the image and scrolling
        // across, while appending the vertical pixel data by series of 8.
        for (int y = 0; y < height; y += 8) {
            for (int x = 0; x < width; x++) {
                int bytePos = offset + x;
                int page = (y / 8);

                for (int k = 0; k < 8; k++) {
                    if ((k + y < height) && (bytePos < buffer.length)) {
                        int pixel = image.getRGB(x, y + k);
                        // Look at Alpha channel instead
                        if ((pixel & 0xFF) > GRADIENT_CUTOFF) {
                            buffer[page][bytePos] |= (byte) (1 << k);
                        }

                    }
                }
            }
        }
    }

    void setImageData(Sh1106Display display, BufferedImage image, int xOffset, int yOffset){
        for(int x = xOffset; x < display.getLcdWidth(); x++){
            for(int y = yOffset; y < display.getLcdHeight(); y++){
                int pixel = image.getRGB(x, y);

                if ((pixel & 0xFF) > GRADIENT_CUTOFF) {
                    display.setPixel(x,y,false);
                }
                else {
                    display.setPixel(x,y, true);
                }
            }
        }
    }
}