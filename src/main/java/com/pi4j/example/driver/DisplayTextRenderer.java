package com.pi4j.example.driver;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * A helper class to display text on @link {@link java.awt.image.BufferedImage}.
 */
public class DisplayTextRenderer {

    public BufferedImage getRenderedTextImage(String text, int displayWidth, int displayHeight){
        var options = TextRenderOptions.builder()
                .displayWidth(displayWidth)
                .displayHeight(displayHeight)
                .build();
        return getRenderedTextImage(text, options);
    }

    public BufferedImage getRenderedTextImage(String text, TextRenderOptions options){
        var image = createImageWithBackgroundColor(options.getDisplayWidth(), options.getDisplayHeight(), options.getBackgroundColor());

        if(text == null || text.isBlank()){
            return image;
        }

        drawTextOnImage(text, options, image);
        image.flush();

        return image;
    }

    private void drawTextOnImage(String text, TextRenderOptions options, BufferedImage image) {
        Font font = options.getFont();
        int displayWidth = options.getDisplayWidth();
        Color textColor = options.getTextColor();


        Graphics2D graphics = image.createGraphics();
        graphics.setFont(font);
        graphics.setColor(textColor);

        var fontMetrics  = graphics.getFontMetrics(font);

        List<String> lines = getLinesUsingFontmetrics(text, fontMetrics::stringWidth, displayWidth, options.getLineBreakOptions());

        int lineHeight = fontMetrics.getHeight();

        int x = options.getStartX();
        int y = options.getStartY() + lineHeight;
        int lineSpacing = options.getLineSpacing();

        for(String line: lines){
            graphics.drawString(line, x,y);
            y += lineHeight + lineSpacing;
        }
        graphics.dispose();
    }

    private List<String> getLinesUsingFontmetrics(String text, Function<String, Integer> calculateStringWidth, int displayWidth, LineBreakOptions lineBreakOptions) {
        switch (lineBreakOptions){
            case ONLY_SYSTEM_LINE_BREAKS:
                return breakTextIntoLinesUsingSystemlinebreaks(text);
            case BREAK_WORD_AT_END_OF_LINE:{
                String[] words = text.split(" ");
                return breakAtEndOfLine(calculateStringWidth, displayWidth, words);
            }
            case AUTO: {
                var lines = new LinkedList<String>();
                for (String systemBreakLine : breakTextIntoLinesUsingSystemlinebreaks(text)) {
                    String[] words = systemBreakLine.split(" ");
                    lines.addAll(breakAtEndOfLine(calculateStringWidth, displayWidth, words));
                }
                return lines;
            }
        }
        throw new IllegalArgumentException("A LineBreakOption is necessary to render text.");
    }

    private static List<String> breakTextIntoLinesUsingSystemlinebreaks(String text) {
        return text.lines().collect(Collectors.toList());
    }

    List<String> breakAtEndOfLine(Function<String, Integer> calculateStringWidth, int displayWidth, String[] words) {
        List<String> lines = new LinkedList<>();

        String line = "";
        boolean firstLine = true;
        for(String word: words){
            if(calculateStringWidth.apply(line + " "+word) > displayWidth){
                if(!firstLine){
                    lines.add(line);
                }
                firstLine = false;
                line = word;
            }
            else if(calculateStringWidth.apply(line) > displayWidth){
                lines.add(line);
                firstLine = false;
                line = "";
            }
            else {
                if(firstLine){
                    line += word;
                    firstLine = false;
                }
                else {
                    line += " "+word;
                }
            }
        }
        lines.add(line);
        return lines;
    }

    private BufferedImage createImageWithBackgroundColor(int displayWidth, int displayHeight, Color backgroundColor) {
        BufferedImage image = new BufferedImage(displayWidth, displayHeight, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D graphics = image.createGraphics();

        graphics.setColor(backgroundColor);
        graphics.fillRect(0,0,displayWidth,displayHeight);
        graphics.dispose();
        return image;
    }
}
