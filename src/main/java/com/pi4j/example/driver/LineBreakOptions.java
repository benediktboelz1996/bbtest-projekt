package com.pi4j.example.driver;

public enum LineBreakOptions {
    AUTO,
    ONLY_SYSTEM_LINE_BREAKS,
    BREAK_WORD_AT_END_OF_LINE
}