package com.pi4j.example.driver;

import java.io.Closeable;

/**
 * Driver for controlling the SH1106 OLED display.
 */
public interface Sh1106Display extends Closeable {
    int DEFAULT_WIDTH = 128;
    int DEFAULT_HEIGHT = 64;

    /**
     * default I2C address for this peripheral
     */
    int I2C_ADDRESS = 0x3C;

    /**
     * alternate I2C address for the display.
     * But typically @link{I2C_ADDRESS} is used.
     */
    int I2C_ADDRESS_ALT = 0x3D;

    /**
     * Sets a specific pixel in the display buffer to on or off. </br>
     * This will be rendered the next time
     * {@link #render()} is called.
     *
     * @param x  The horizontal coordinate.
     * @param y  The vertical coordinate.
     * @param on Set to true to enable the pixel; false to disable the pixel.
     * @throws IllegalArgumentException if the x or y coordinage is out of range.
     */
    void setPixel(int x, int y, boolean on) throws IllegalArgumentException;

    /**
     * Renders the current pixel data to the screen.
     */
    void render();

    /**
     * @return the width of the display
     */
    int getLcdWidth();

    /**
     * @return the height of the display
     */
    int getLcdHeight();

    /**
     * Clears all pixel data in the display buffer. <br />
     * This will be rendered the next time
     * {@link #render()} is called.
     */
    void clearPixels();

    /**
     * Sets text to render to the display.
     * @param text the text. Will use default font and breaks lines automatically.
     */
    void setText(String text);

    /**
     * Sets text to render to the display.
     * @param text the text. Will use default font and breaks lines automatically.
     * @param options the options. width and height default to display.
     */
    void setText(String text, TextRenderOptions options);
}
