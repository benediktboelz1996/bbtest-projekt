package com.pi4j.example.driver;/*
 * Copyright 2018 Roberto Leinardi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.pi4j.io.i2c.I2C;

import java.awt.image.BufferedImage;
import java.io.Closeable;
import java.io.IOException;
import java.util.Arrays;

/**
 * Driver for controlling the SH1106 OLED display.
 */
@SuppressWarnings("WeakerAccess")
public class Sh1106DisplayImpl implements Sh1106Display {
    private static final String TAG = Sh1106DisplayImpl.class.getSimpleName();

    private static final int PAGES = 8;
    private static final int VERTICAL_PIXEL_PER_PAGE = 8;

    // Protocol constants
    private static final int DATA_OFFSET = 1;
    private static final int COMMAND_DISPLAY_ON = 0xAF;
    private static final int COMMAND_DISPLAY_OFF = 0xAE;
    private static final int COMMAND_MEMORY_ADDRESSING_MODE = 0x20;
    private static final int COMMAND_HIGH_COLUMN = 0x10;
    private static final int COMMAND_PAGE = 0xB0;
    private static final int COMMAND_COMMON_OUTPUT_SCAN_DIRECTION = 0xC8;
    private static final int COMMAND_LOW_COLUMN = 0x02;
    private static final int COMMAND_DISPLAY_START_LINE = 0x40;
    private static final int COMMAND_SEGMENT_REMAP = 0xA1;
    private static final int COMMAND_NORMAL_DISPLAY = 0xA6;
    private static final int COMMAND_INVERTED_DISPLAY = 0xA7;
    private static final int COMMAND_DISPLAY_OFFSET = 0xD3;
    private static final int COMMAND_DISPLAY_OFFSET_VALUE = 0x00;
    private static final int COMMAND_DISPLAY_CLOCK_DIV = 0xD5;
    private static final int COMMAND_DISPLAY_CLOCK_DIV_VALUE = 0xF0;
    private static final int COMMAND_PRE_CHARGE = 0xD9;
    private static final int COMMAND_PRE_CHARGE_VALUE = 0x22;
    private static final int COMMAND_COMMON_PADS_HARDWARE_CONFIGURATION = 0xDA;
    private static final int COMMAND_COMMON_PADS_HARDWARE_CONFIGURATION_VALUE = 0x12;
    private static final int COMMAND_VCOM_DESELECT_LEVEL = 0xDB;
    private static final int COMMAND_VCOM_DESELECT_LEVEL_VALUE = 0x20;
    private static final int COMMAND_CHARGE_PUMP = 0x8D;
    private static final int COMMAND_CONTRAST_LEVEL = 0x81;

    private static final byte[] INIT_PAYLOAD = new byte[]{
            0, (byte) COMMAND_DISPLAY_OFF,
            0, (byte) COMMAND_MEMORY_ADDRESSING_MODE,
            0, (byte) COMMAND_HIGH_COLUMN,
            0, (byte) COMMAND_PAGE,
            0, (byte) COMMAND_COMMON_OUTPUT_SCAN_DIRECTION,
            0, (byte) COMMAND_LOW_COLUMN,
            0, (byte) COMMAND_SEGMENT_REMAP,
            0, (byte) COMMAND_INVERTED_DISPLAY,
            0, (byte) COMMAND_DISPLAY_START_LINE,
            0, (byte) COMMAND_DISPLAY_OFFSET,
            0, (byte) COMMAND_DISPLAY_OFFSET_VALUE,
            0, (byte) COMMAND_DISPLAY_CLOCK_DIV,
            0, (byte) COMMAND_DISPLAY_CLOCK_DIV_VALUE,
            0, (byte) COMMAND_PRE_CHARGE,
            0, (byte) COMMAND_PRE_CHARGE_VALUE,
            0, (byte) COMMAND_COMMON_PADS_HARDWARE_CONFIGURATION,
            0, (byte) COMMAND_COMMON_PADS_HARDWARE_CONFIGURATION_VALUE,
            0, (byte) COMMAND_VCOM_DESELECT_LEVEL,
            0, (byte) COMMAND_VCOM_DESELECT_LEVEL_VALUE,
            0, (byte) COMMAND_DISPLAY_ON,
            0, (byte) COMMAND_CHARGE_PUMP,
    };

    private I2C mI2cDevice;

    // Screen dimension.
    private int mWidth;
    private int mHeight;

    // Holds the i2c payloads.
    private byte[][] mBuffer;
    private final BitmapHelper bmpHelper = new BitmapHelper();

    private final DisplayTextRenderer textRenderer = new DisplayTextRenderer();

    /**
     * Create a new Sh1106 driver connected to the given device
     *
     * @param device I2C device of the display
     */
    public Sh1106DisplayImpl(I2C device) {
        init(device, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public Sh1106DisplayImpl(I2C device, int width, int height) throws IOException {
        init(device, width, height);
    }

    private Sh1106DisplayImpl(I2C device, int width, int height, byte[][] buffer){
        this.mI2cDevice = device;
        this.mWidth = width;
        this.mHeight = height;
        this.mBuffer = buffer;
    }

    /**
     * A copy of the current screen.
     * !Attention: I2C should only be used on the original device _or_ the copy.
     * @return the copied device.
     */
    Sh1106DisplayImpl getCopyOfThis(){
        return new Sh1106DisplayImpl(this.mI2cDevice, this.mWidth, this.mHeight, this.getCopyOfInternalScreenBuffer());
    }

    /**
     * Copy of i2c-device
     * @param device
     * @param mBuffer
     */
    Sh1106DisplayImpl(I2C device, byte[][] mBuffer){

    }

    /**
     * Recommended start sequence for initializing the communications with the OLED display.
     * WARNING: If you change this code, power cycle your display before testing.
     *
     */
    private void init(I2C device, int width, int height) {
        if(device == null) {
            throw new IllegalArgumentException("I2CDevice must not be null.");
        }

        mI2cDevice = device;
        mWidth = width;
        mHeight = height;
        mBuffer = new byte[PAGES][(((mWidth * mHeight) / VERTICAL_PIXEL_PER_PAGE) / PAGES) + DATA_OFFSET];

        var image = new BufferedImage(mWidth, mHeight, BufferedImage.TYPE_4BYTE_ABGR);
        bmpHelper.bmpToBytes(
                mBuffer,
                DATA_OFFSET,
                image);
        for (byte[] page : mBuffer) {
            page[0] = (byte) COMMAND_DISPLAY_START_LINE;
        }
        mI2cDevice.write(INIT_PAYLOAD, INIT_PAYLOAD.length);
    }

    @Override
    public void close() throws IOException {
        if (mI2cDevice != null) {
            try {
                setDisplayOn(false);
                mI2cDevice.close();
            } finally {
                mI2cDevice = null;
            }
        }
    }

    public int getLcdWidth() {
        return mWidth;
    }

    public int getLcdHeight() {
        return mHeight;
    }

    /**
     * Clears all pixel data in the display buffer. This will be rendered the next time
     * {@link #render()} is called.
     */
    public void clearPixels() {
        for (byte[] row : mBuffer) {
            Arrays.fill(row, DATA_OFFSET, row.length, (byte) 0);
        }
    }

    @Override
    public void setText(String text) {
        var textAsImage = textRenderer.getRenderedTextImage(text, getLcdWidth(), getLcdHeight());
        synchronized (this){
            bmpHelper.setImageData(this, textAsImage,0,0);
        }
    }

    @Override
    public void setText(String text, TextRenderOptions options) {
        options.setDisplayWidth(mWidth);
        options.setDisplayHeight(mHeight);
        var textAsImage = textRenderer.getRenderedTextImage(text, options);
        synchronized (this){
            bmpHelper.setImageData(this, textAsImage,0,0);
        }
    }


    public synchronized void setPixel(int x, int y, boolean on) throws IllegalArgumentException {
        if (x < 0 || y < 0 || x >= mWidth || y >= mHeight) {
            throw new IllegalArgumentException("Pixel out of bound: x=" + x + ", y=" + y);
        }
        if (on) {
            mBuffer[(y / PAGES)][DATA_OFFSET + x] |= (byte) (1 << y % VERTICAL_PIXEL_PER_PAGE);
        } else {
            mBuffer[(y / PAGES)][DATA_OFFSET + x] &= (byte) ~(1 << y % VERTICAL_PIXEL_PER_PAGE);
        }
    }

    /**
     * Sets the contrast for the display.
     *
     * @param level The contrast level (0-255).
     * @throws IllegalStateException
     * @throws IllegalArgumentException
     */
    public synchronized void setContrast(int level) throws IllegalArgumentException {
        if (level < 0 || level > 255) {
            throw new IllegalArgumentException("Invalid contrast " + level +
                    ", level must be between 0 and 255");
        }
        mI2cDevice.writeRegister(0, (byte) COMMAND_CONTRAST_LEVEL);
        mI2cDevice.writeRegister(0, (byte) level);
    }

    /**
     * Turns the display on and off.
     *
     * @param on Set to true to enable the display; set to false to disable the display.
     * @throws IOException
     * @throws IllegalStateException
     */
    public synchronized void setDisplayOn(boolean on) throws IOException, IllegalStateException {
        if (mI2cDevice == null) {
            throw new IllegalStateException("I2C Device not open");
        }
        if (on) {
            mI2cDevice.writeRegister(0, (byte) COMMAND_DISPLAY_ON);
        } else {
            mI2cDevice.writeRegister(0, (byte) COMMAND_DISPLAY_OFF);
        }
    }

    /**
     * Invert the display color without rewriting the contents of the display data RAM..
     *
     * @param invert Set to true to invert the display color; set to false to set the display back to normal.
     */
    public synchronized void setInvertDisplay(boolean invert) throws IllegalStateException {

        if (invert) {
            mI2cDevice.writeRegister(0, (byte) COMMAND_INVERTED_DISPLAY);
        } else {
            mI2cDevice.writeRegister(0, (byte) COMMAND_NORMAL_DISPLAY);
        }
    }

    public synchronized void renderImage(BufferedImage image){
        //bmpHelper.setBmpData(this,0,0,image);
        bmpHelper.setImageData(this, image, 0,0);
        render();
    }

    public synchronized void render() {
        for (int page = 0; page < PAGES; page++) {
            mI2cDevice.writeRegister(0, (byte) (COMMAND_PAGE + page));
            mI2cDevice.writeRegister(0, (byte) COMMAND_HIGH_COLUMN);
            mI2cDevice.writeRegister(0, (byte) COMMAND_LOW_COLUMN);
            mI2cDevice.write(mBuffer[page], mBuffer[page].length);
        }
    }

    private byte[][] getCopyOfInternalScreenBuffer(){
        return Arrays.stream(mBuffer).map(byte[]::clone).toArray(byte[][]::new);
    }
}
