package com.pi4j.example.driver;

import lombok.*;

import javax.sound.sampled.Line;
import java.awt.*;

@Builder
@Getter
@Setter(AccessLevel.PUBLIC)
@ToString
@EqualsAndHashCode
public class TextRenderOptions {
    @Builder.Default
    private Font font = new Font ("FreeSansBold", Font.PLAIN, 14);
    private int displayWidth;
    private int displayHeight;
    @Builder.Default
    private Color textColor = Color.WHITE;
    @Builder.Default
    private java.awt.Color backgroundColor = Color.BLACK;
    private int startX = 0;
    private int startY = 0;
    private int lineSpacing = 0;
    @Builder.Default
    private LineBreakOptions lineBreakOptions = LineBreakOptions.AUTO;

    public void setFontSize(int size){
        this.font = new Font(this.font.getName(), Font.PLAIN, size);
    }

}
