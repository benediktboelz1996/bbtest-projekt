package com.pi4j.example.hardware;

import com.pi4j.example.driver.AnimationAwareSh1106Dispaly;
import com.pi4j.example.driver.Sh1106Display;

public interface ClientHardware {
    AnimationAwareSh1106Dispaly getDisplay();

    /**
     * Adds an action to the button. Multiple actions can be added to a button.
     * @param button the id of the button (1-6). from left to right beginning in left top corner.
     * @param action
     */
    void addOnButtonClick(int button, Runnable action);

    void addOnDoubleClick(int button, Runnable action);
}
