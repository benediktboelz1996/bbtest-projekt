package com.pi4j.example.hardware;

import com.pi4j.context.Context;
import com.pi4j.example.driver.AnimationAwareSh1106Dispaly;
import com.pi4j.example.driver.AnimationAwareSh1106DisplayImpl;
import com.pi4j.example.driver.Sh1106DisplayImpl;
import com.pi4j.io.gpio.digital.DigitalInput;
import com.pi4j.io.gpio.digital.PullResistance;
import com.pi4j.io.i2c.I2C;
import com.pi4j.io.i2c.I2CConfig;
import com.pi4j.io.i2c.I2CProvider;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class ClientHardwareImpl implements  ClientHardware {

    private AnimationAwareSh1106Dispaly display;

    private final Map<Integer, DblClickableButton> buttonById = new ConcurrentHashMap<Integer, DblClickableButton>();

    /**
     *
     * @param context An initialized context
     * @param pinNumbers the bcm pin number of the 6 buttons. starting from left to right and top to bottom.
     *                   e.g. top left = 1, bottom right = 6
     */
    public ClientHardwareImpl(Context context, int[] pinNumbers) {
        initDisplay(context);
        initButtons(context, pinNumbers);
    }



    private void initButtons(Context context, int[] pinNumbers) {
        if(pinNumbers.length != 6){
            throw new IllegalArgumentException("There are exactly 6 pin numbers (bcm-number) expected to init the hardware-buttons. actual: "+pinNumbers.length);
        }
        log.info("Using bcmAddresses as buttons>{}<", Arrays.toString(pinNumbers));

        for(int i=0; i< pinNumbers.length; i++){
            int bcmAddress = pinNumbers[i];

            var buttonConfig = DigitalInput.newConfigBuilder(context)
                    .id("button-"+(i+1))
                    .name("Button "+(i+1))
                    .address(bcmAddress)
                    .pull(PullResistance.PULL_DOWN)
                    .debounce(3000L)
                    .provider("pigpio-digital-input");
            var button = context.create(buttonConfig);

            buttonById.put(i, new DblClickableButton(button));
        }

    }

    private void initDisplay(Context context) {
        I2CProvider i2CProvider = context.provider("pigpio-i2c");
        I2CConfig i2cConfigBuild = I2C.newConfigBuilder(context)
                .id("SH1106-Display")
                .bus(1)
                .device(0x3C)
                .build();
        var i2cConfig = i2CProvider.create(i2cConfigBuild);

        display = new AnimationAwareSh1106DisplayImpl(new Sh1106DisplayImpl(i2cConfig));
    }

    @Override
    public AnimationAwareSh1106Dispaly getDisplay() {
        return display;
    }

    @Override
    public void addOnButtonClick(int buttonId, Runnable action) {
        validateButtonId(buttonId);
        var button = buttonById.get(buttonId - 1);
        button.addOnSingleClick(action);

    }

    @Override
    public void addOnDoubleClick(int buttonId, Runnable action) {
        validateButtonId(buttonId);
        var button = buttonById.get(buttonId - 1);
        button.addOnDoubleClick(action);
    }

    private static void validateButtonId(int buttonId) {
        if(buttonId < 1 || buttonId > 6){
            throw new IllegalArgumentException("buttonId must be in range [1-6]");
        }
    }
}
