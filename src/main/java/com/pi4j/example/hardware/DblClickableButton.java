package com.pi4j.example.hardware;

import com.pi4j.io.gpio.digital.DigitalInput;
import com.pi4j.io.gpio.digital.DigitalState;

import java.util.*;

public class DblClickableButton {
    private final DigitalInput button;

    private final Timer timer = new Timer();

    final List<Runnable> onSingleClick = Collections.synchronizedList(new LinkedList<>());

    final List<Runnable> onDoubleClick = Collections.synchronizedList(new LinkedList<>());

    private volatile long lastTimeClicked = 0L;

    volatile SingleClickExecutionTask singleClickExecutionTask = null;

    private int delayDoubleClickInMs = 300;

    public DblClickableButton(DigitalInput button) {
        this.button = button;

        init();
    }

    private void init() {
        button.addListener(e -> {
            if (e.state() == DigitalState.LOW) {
                // einfach click nach delay.
                if(singleClickExecutionTask == null){
                    lastTimeClicked = System.currentTimeMillis();
                    System.out.println("single clicked");
                    singleClickExecutionTask = new SingleClickExecutionTask(this);
                    timer.schedule(singleClickExecutionTask, delayDoubleClickInMs);
                }
                else {
                    //dbl click
                    if(System.currentTimeMillis() - lastTimeClicked < delayDoubleClickInMs){
                        System.out.println("double clicked");
                        singleClickExecutionTask.cancel();
                        singleClickExecutionTask = null;
                        synchronized (onDoubleClick){
                            for(var action: onDoubleClick){
                                action.run();
                            }
                        }
                    }
                }

            }
            else {
                //nothing to do. pi4j triggers 2 events per button click.
            }
        });


    }

    public void addOnSingleClick(Runnable action){
        this.onSingleClick.add(action);
    }

    public void addOnDoubleClick(Runnable action){
    this.onDoubleClick.add(action);
    }
}

class SingleClickExecutionTask extends TimerTask {
    private final DblClickableButton button;

    public SingleClickExecutionTask(DblClickableButton button) {
        this.button = button;
    }

    @Override
    public void run() {
        System.out.println("run single click");
        button.singleClickExecutionTask = null;
        synchronized (button.onSingleClick){
            for(var action: button.onSingleClick){
                action.run();
            }
        }

    }
}