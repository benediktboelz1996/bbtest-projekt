package com.pi4j.example.logic;

import lombok.Data;

/**
 * represents a game that is currently played.
 */
@Data
public class CurrentGameData {
    private int toreTeamWeiss = 0;
    private int toreTeamRot = 0;

    private String teamnameRot = "";
    private String teamnameWeiss = "";

    private int teamIDRot = -1;
    private int teamIDWeiss = -1;
}
