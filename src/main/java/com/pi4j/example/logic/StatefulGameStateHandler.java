package com.pi4j.example.logic;

import com.pi4j.example.animations.ShowTextEnlargingAnimation;
import com.pi4j.example.driver.AnimationAwareSh1106Dispaly;
import com.pi4j.example.driver.LineBreakOptions;
import com.pi4j.example.driver.TextRenderOptions;
import com.pi4j.example.hardware.ClientHardware;
import de.shgruppe.tischkicker.miniclient.TischkickerService;
import lombok.extern.slf4j.Slf4j;
import tischkicker.messages.SpielErgebnis;
import tischkicker.models.Spiel;
import tischkicker.models.Team;
import tischkicker.models.Tor;

@Slf4j
public class StatefulGameStateHandler {

    private final ClientHardware hardware;
    private final TischkickerService tischkickerService;

    private final AnimationAwareSh1106Dispaly display;

    private volatile GAME_STATE state = GAME_STATE.INITIAL;

    private CurrentGameData currentGame = null;

    private boolean showTeamNames = false;

    public StatefulGameStateHandler(ClientHardware hardware, TischkickerService service){
        this.hardware = hardware;
        this.display = hardware.getDisplay();
        this.tischkickerService = service;
    }

    public void init(){
        TextRenderOptions options = TextRenderOptions.builder()
                .lineBreakOptions(LineBreakOptions.ONLY_SYSTEM_LINE_BREAKS)
                .build();
        hardware.getDisplay().setText("let's  -  go\n<               >", options);
        display.render();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        hardware.addOnDoubleClick(1, () -> {
            log.info("try to reconnect");
            tischkickerService.reconnectIfNecessary();
            //weil der WEBSOCKET evtl. nicht verfügbar war, gehen wir in den initial-Zustand.
            this.state = GAME_STATE.INITIAL;
            log.info("reset state to initial");
        });


        hardware.addOnButtonClick(1, () -> {
            switch(state) {
                case INITIAL:{
                    display.setText("FI-AE-22\nWarte auf Spieldaten.");
                    display.render();
                }
                case A_GAME_IS_RUNNING: {
                    renderCurrentGameData();
                    showTeamNames = !showTeamNames;
                }
            }
        });

        hardware.addOnButtonClick(2, () -> {
            //display.setText("FI-AE-22\nTischkicker\nGeil!!!");
            //display.render();
        });

        hardware.addOnDoubleClick(2, () -> {
            display.playAnimation(new ShowTextEnlargingAnimation("FI-AE-22"));
        });

        tischkickerService.setSpielBeendetCallback(spielBeendetMessage -> {
            log.info("bekam ein spielbeendet-callback");
            this.state = GAME_STATE.INITIAL;
            this.currentGame = null;
        });

        tischkickerService.setSpielErgebnisCallback(ergebnis -> {

            boolean isSeitenwechsel = isSeitenwechsel(ergebnis, currentGame);

            log.info("bekam ein erbenis-callback");
            this.state = GAME_STATE.A_GAME_IS_RUNNING;
            updateAndRenderCurrentGameData(ergebnis);
            if(!isSeitenwechsel){
                this.display.playAnimation(new ShowTextEnlargingAnimation("Toooor!"));
            }
        });

        hardware.addOnButtonClick(3, () -> {
            if(isGameRunning()){
                int teamID = getTeamSeiteWeiss();
                log.info("Spielstand inkrementieren team"+teamID);
                if(teamID > 0){
                    tischkickerService.spielstandInkrementieren(teamID);
                }
            }
        });

        hardware.addOnButtonClick(5, () -> {
            if(isGameRunning()){
                int teamID = getTeamSeiteWeiss();
                log.info("Spielstand dekrementieren team"+teamID);
                if(teamID > 0){
                    tischkickerService.spielstandDekrementieren(teamID);
                }
            }
        });

        hardware.addOnButtonClick(4, () -> {
            if(isGameRunning()){
                int teamID = getTeamSeiteRot();
                log.info("Spielstand inkrementieren team"+teamID);
                if(teamID > 0){
                    tischkickerService.spielstandInkrementieren(teamID);
                }
            }
        });

        hardware.addOnButtonClick(6, () -> {
            if(isGameRunning()){
                int teamID = getTeamSeiteRot();
                log.info("Spielstand dekrementieren team"+teamID);
                if(teamID > 0){
                    tischkickerService.spielstandDekrementieren(teamID);
                }
            }
        });
    }

    private boolean isSeitenwechsel(SpielErgebnis ergebnis, CurrentGameData data) {
        if(data == null){
            return false;
        }
        // wenn die Anzahl Tore abweicht ist es ein Seitenwechsel.
        int toreNeu = ergebnis.toreTeam1 + ergebnis.toreTeam2;
        int toreAlt = data.getToreTeamRot() + data.getToreTeamWeiss();

        if(toreNeu == toreAlt){
            return true;
        }

        return false;
    }

    private void renderCurrentGameData() {
        if(showTeamNames){
            renderSpielStandMitTeamNamen();
        }
        else {
            renderSpielStandOhneTeamnamen();
        }
    }

    private void renderSpielStandMitTeamNamen() {
        if(currentGame != null){
            TextRenderOptions options = TextRenderOptions.builder()
                    .lineBreakOptions(LineBreakOptions.ONLY_SYSTEM_LINE_BREAKS)
                    .build();

            String line1 = "(weiss) " + currentGame.getTeamnameWeiss();
            String line2 = currentGame.getToreTeamWeiss() + " : " + currentGame.getToreTeamRot();
            String line3 = "(rot) " + currentGame.getTeamnameRot();
            display.setText(line1 + "\n" + line2 + "\n"+line3, options);
            display.render();
        }
    }

    private void updateAndRenderCurrentGameData(SpielErgebnis ergebnis) {
        log.info("update game data");
        if(currentGame == null){
            currentGame = new CurrentGameData();
        }
        if(ergebnis.teams == null || ergebnis.teams.length !=  2){
            log.warn("Teams are null or not length 2");
            return;
        }

        Team team1 = ergebnis.teams[0];
        Team team2 = ergebnis.teams[1];

        int toreTeam1 = ergebnis.toreTeam1;
        int toreTeam2 = ergebnis.toreTeam2;

        if(Boolean.valueOf(System.getProperty("invert-teams", "false"))){
            Team origTeam1 = team1;
            team1 = team2;
            team2 = origTeam1;

            int toreTeam1Tmp = toreTeam1;
            toreTeam1 = toreTeam2;
            toreTeam2 = toreTeam1Tmp;
        }

        int team1ID = team1.getId();
        int team2ID = team2.getId();
        String team1Name = team1.getName();
        String team2Name = team2.getName();

        if(Tor.Seite.WEISS == ergebnis.seiteTeam1){
            currentGame.setTeamIDWeiss(team1ID);
            currentGame.setTeamIDRot(team2ID);
            currentGame.setToreTeamWeiss(toreTeam1);
            currentGame.setToreTeamRot(toreTeam2);
            currentGame.setTeamnameWeiss(team1Name);
            currentGame.setTeamnameRot(team2Name);
        }
        else {
            currentGame.setTeamIDWeiss(team2ID);
            currentGame.setTeamIDRot(team1ID);
            currentGame.setToreTeamWeiss(toreTeam2);
            currentGame.setToreTeamRot(toreTeam1);
            currentGame.setTeamnameWeiss(team2Name);
            currentGame.setTeamnameRot(team1Name);
        }

        renderCurrentGameData();
    }

    private void renderSpielStandOhneTeamnamen() {
        if(currentGame != null){
            TextRenderOptions options = TextRenderOptions.builder()
                    .build();
            options.setFontSize(30);
            display.setText(currentGame.getToreTeamWeiss() + " : " + currentGame.getToreTeamRot(), options);
            display.render();
        }
    }

    private int getTeamSeiteRot() {
        if(currentGame != null){
            return currentGame.getTeamIDRot();
        }
        return -1;
    }

    private int getTeamSeiteWeiss() {
        if(currentGame != null){
            return currentGame.getTeamIDWeiss();
        }
        return -1;
    }

    private boolean isGameRunning(){
        return state == GAME_STATE.A_GAME_IS_RUNNING;
    }
}

enum GAME_STATE {
    /*
     * in initial state you cannot choose a game as the players and teams need to init.
     */
    INITIAL,
    A_GAME_IS_RUNNING,
    CHOOSE_A_GAME,
    ALL_GAMES_FINISHED
}