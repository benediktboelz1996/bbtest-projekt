package de.shgruppe.tischkicker.miniclient;

import lombok.extern.slf4j.Slf4j;
import tischkicker.messages.SpielBeendetMessage;
import tischkicker.messages.SpielErgebnis;
import tischkicker.models.Spiel;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

@Slf4j
public class MockTischkickerService implements TischkickerService{
    @Override
    public List<Spiel> getSpielbareSpiele() {
        return new LinkedList<>();
    }

    @Override
    public void starteSpiel(int spielId) {
        log.info("Spiel mit ID {} gestartet", spielId);
    }

    @Override
    public void spielstandInkrementieren(int teamId) {
        log.info("inkrementieren");
    }

    @Override
    public void spielstandDekrementieren(int teamId) {
        log.info("dekrementieren");
    }

    @Override
    public void seitenwechsel() {
        log.info("seitenwechsel");
    }

    @Override
    public void setSpielErgebnisCallback(Consumer<SpielErgebnis> spielErgebnisCallback) {

    }

    @Override
    public void setSpielBeendetCallback(Consumer<SpielBeendetMessage> spielBeendetCallback) {

    }

    @Override
    public void reconnectIfNecessary() {

    }
}
