package de.shgruppe.tischkicker.miniclient;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import tischkicker.messages.SpielBeendetMessage;
import tischkicker.messages.SpielErgebnis;
import tischkicker.models.Spiel;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;


// TODO: Generell besseres Errorhandling

/**
 * Kommunikationsschnittstelle zwischen Client und dem Tischkicker-Server
 */
@Slf4j
public class TischKickerServiceImpl implements TischkickerService {
    private static final String BASE_URL = "http://localhost:8080";
    private final HttpClient httpClient = HttpClient.newHttpClient();
    private TischkickerWebSocket tischkickerWebSocket;

    private Consumer<SpielErgebnis> spielErgebnisCallback;

    private Consumer<SpielBeendetMessage> spielBeendetCallback;

    public TischKickerServiceImpl() {
        reconnectIfNecessary();
    }

    public HttpRequest.Builder createBaseRequest(String ressource) {
        return HttpRequest.newBuilder().uri(URI.create(BASE_URL + ressource))
                .header("Content-Type", "application/json")
                .header("Accept", "application/json");
    }

    /**
     * Holt per GET eine Ressource
     * @param ressource Pfad der Ressource ('/' muss vorangestellt werden)
     * @param clazz Klasse des Ergebnisobjekts
     * @return Ergebnisobjekt aus Body der GET-Anfrage oder null
     * @param <T> Typ des Ergebnisobjekts
     */
    public <T> T getRessource(String ressource, Class<T> clazz) {
        T result = null;

        HttpRequest request = createBaseRequest(ressource).GET().build();
        HttpResponse<String> response = null;

        try {
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        if(response != null && response.statusCode() == 200) {
            Gson gson = new Gson();

            result = gson.fromJson(response.body(), clazz);
        } else if(response != null){
            System.out.println("Status != 200: " + response.statusCode());
            System.out.println(response);
        }

        return result;
    }

    /**
     * Ruft per POST die angegebene Ressource mit einem leeren Body auf {@link TischKickerServiceImpl#postRessource(String, Object)}
     * @param ressource Pfad der Ressource ('/' muss vorangestellt werden)
     */
    public void postRessource(String ressource) {
        postRessource(ressource, null);
    }

    /**
     * Schickt per POST Daten an den Server
     * @param ressource Pfad der Ressource ('/' muss vorangestellt werden)
     * @param body Objekt für den Body der Anfrage oder null
     * @param <T> Typ des Body-Objekts
     */
    public <T> void postRessource(String ressource, T body) {
        String bodyJson = "";
        if(body != null) {
            Gson gson = new Gson();
            bodyJson = gson.toJson(body);
        }

        HttpRequest request = createBaseRequest(ressource)
                .POST(HttpRequest.BodyPublishers.ofString(bodyJson, StandardCharsets.UTF_8)).build();
        HttpResponse<String> response = null;

        try {
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        if(response != null && response.statusCode() != 200) {
            System.out.println("Status != 200: " + response.statusCode());
            System.out.println(response);
        }
    }

    @Override
    public List<Spiel> getSpielbareSpiele() {
        Spiel[] spiele = getRessource("/spiele", Spiel[].class);
        return Arrays.stream(spiele).filter(
                spiel -> spiel.getTeamIDs() != null
                        && spiel.getTeamIDs().length > 1
                        && spiel.getTeamIDs()[0] > -1
                        && spiel.getTeamIDs()[1] > -1
                        && spiel.getToreteam1() != 10
                        && spiel.getToreteam2() != 10)
                .collect(Collectors.toList());
    }

    @Override
    public void starteSpiel(int spielId) {
        postRessource("/spiel/start/" + spielId);
    }

    @Override
    public void spielstandInkrementieren(int teamId) {
        postRessource("/spiel/increment/" + teamId);
    }

    @Override
    public void spielstandDekrementieren(int teamId) {
        postRessource("/spiel/decrement/" + teamId);
    }

    @Override
    public void seitenwechsel() {
        postRessource("/spiel/seitenwechsel");
    }

    @Override
    public void setSpielErgebnisCallback(Consumer<SpielErgebnis> spielErgebnisCallback) {
        this.spielErgebnisCallback = spielErgebnisCallback;

        if(tischkickerWebSocket != null){
            tischkickerWebSocket.setSpielErgebnisCallback(spielErgebnisCallback);
        }
    }

    @Override
    public void setSpielBeendetCallback(Consumer<SpielBeendetMessage> spielBeendetCallback) {
        this.spielBeendetCallback = spielBeendetCallback;

        if(tischkickerWebSocket != null){
            tischkickerWebSocket.setSpielBeendetCallback(spielBeendetCallback);
        }
    }

    @Override
    public void reconnectIfNecessary() {
        if(tischkickerWebSocket == null){
            try {
                tischkickerWebSocket = new TischkickerWebSocket();
                tischkickerWebSocket.connect();
                tischkickerWebSocket.setSpielBeendetCallback(this.spielBeendetCallback);
                tischkickerWebSocket.setSpielErgebnisCallback(this.spielErgebnisCallback);
            } catch (Exception e) {
                tischkickerWebSocket = null;
                log.warn("could not connect", e);
            }
        }
    }
}
