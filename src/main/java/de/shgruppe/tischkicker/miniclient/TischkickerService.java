package de.shgruppe.tischkicker.miniclient;

import tischkicker.messages.SpielBeendetMessage;
import tischkicker.messages.SpielErgebnis;
import tischkicker.models.Spiel;

import java.util.List;
import java.util.function.Consumer;

/**
 * Kommunikationsschnittstelle zwischen Client und dem Tischkicker-Server
 */
public interface TischkickerService {
    /**
     * @return Spiele die bereit sind gespielt zu werden
     */
    List<Spiel> getSpielbareSpiele();

    /**
     * Startet ein Spiel
     * @param spielId ID des Spiels
     */
    void starteSpiel(int spielId);

    /**
     * Inkrementiert den Spielstand für das angegebene Team
     * @param teamId ID des Teams
     */
    void spielstandInkrementieren(int teamId);

    /**
     * Dekrementiert den Spielstand für das angegebene Team
     * @param teamId ID des Teams
     */
    void spielstandDekrementieren(int teamId);

    /**
     * Leitet im laufenden Spiel einen Seitenwechsel ein.
     * Soweit bekannt nur einmal im Spiel möglich
     */
    void seitenwechsel();

    // TODO: Aufgeben

    /**
     * Setzt einen Callback für über die WebSocket-Verbindung kommenden Spielergebnisse
     * @param spielErgebnisCallback der Callback
     */
    void setSpielErgebnisCallback(Consumer<SpielErgebnis> spielErgebnisCallback);

    /**
     * Setzt einen Callback für über die WebSocket-Verbindung kommenden beendeten Spiele
     * @param spielBeendetCallback der Callback
     */
    void setSpielBeendetCallback(Consumer<SpielBeendetMessage> spielBeendetCallback);

    void reconnectIfNecessary();
}
