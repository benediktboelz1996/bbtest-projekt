package de.shgruppe.tischkicker.miniclient;

import com.google.gson.Gson;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import tischkicker.messages.Message;
import tischkicker.messages.MessageType;
import tischkicker.messages.SpielBeendetMessage;
import tischkicker.messages.SpielErgebnis;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;
import java.util.function.Consumer;

public class TischkickerWebSocket extends WebSocketClient {
    private static final String SOCKET_URI = "ws://localhost:8080/live";
    private Consumer<SpielErgebnis> spielErgebnisCallback = spielErgebnis -> System.out.println("Kein Callback für Spielergebnisse gesetzt");
    private Consumer<SpielBeendetMessage> spielBeendetCallback = spielBeendet -> System.out.println("Kein Callback für beendete Spiele gesetzt");

    public TischkickerWebSocket() throws URISyntaxException {
        super(new URI(SOCKET_URI));
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        System.out.println("WebSocket Verbindung offen");
    }

    @Override
    public void onMessage(String s) {
        Gson gson = new Gson();
        Message deserializedMessage = gson.fromJson(s, Message.class);

        if(deserializedMessage != null) {
            if(Objects.equals(deserializedMessage.type, MessageType.SpielErgebnis)) {
                SpielErgebnis spielErgebnis = gson.fromJson(s, SpielErgebnis.class);
                spielErgebnisCallback.accept(spielErgebnis);
            } else if(Objects.equals(deserializedMessage.type, MessageType.SpielBeendet)) {
                SpielBeendetMessage spielBeendet = gson.fromJson(s, SpielBeendetMessage.class);
                spielBeendetCallback.accept(spielBeendet);
            } else if(Objects.equals(deserializedMessage.type, MessageType.Phasenaenderung)) {
                // TODO: Was soll hier passieren? Es gibt hierfür noch keine Objektstruktur
            } else {
                System.out.println("Unbekannter MessageType " + deserializedMessage.type);
                System.out.println(deserializedMessage);
            }
        }

    }

    @Override
    public void onClose(int i, String s, boolean b) {
        System.out.println("WebSocket Verbindung geschlossen");
    }

    @Override
    public void onError(Exception e) {
        // TODO: Bei Fehlschlag für ein paar Minuten so alle 10 Sekunden neu probieren
        System.out.println("onError");
    }

    public void setSpielErgebnisCallback(Consumer<SpielErgebnis> spielErgebnisCallback) {
        this.spielErgebnisCallback = spielErgebnisCallback;
    }

    public void setSpielBeendetCallback(Consumer<SpielBeendetMessage> spielBeendetCallback) {
        this.spielBeendetCallback = spielBeendetCallback;
    }
}
