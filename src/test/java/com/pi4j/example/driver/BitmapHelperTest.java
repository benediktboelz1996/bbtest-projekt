package com.pi4j.example.driver;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;

public class BitmapHelperTest {
    public static void main(String[] args) {
        BitmapHelper helper= new BitmapHelper();

        var bufferedImage = new BufferedImage(128,64,BufferedImage.TYPE_4BYTE_ABGR);
        var graphics = bufferedImage.getGraphics();

        Font font = new Font("Arial", Font.BOLD, 18);
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0,0,30,30);

        graphics.setColor(Color.BLACK);
        graphics.fillRect(0,0,60,60);
        graphics.setFont(font);
        graphics.drawString("test", 0,0);



        byte[][] buffer = new byte[128][64];

        //helper.bmpToBytes(buffer, 0, bufferedImage, true);

        System.out.println(Arrays.deepToString(buffer));
    }
}
