package com.pi4j.example.driver;

import org.junit.jupiter.api.Test;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DisplayRendererTest {
    private final DisplayTextRenderer renderer = new DisplayTextRenderer();

    @Test
    void breaksLongLines(){
        List<String> lines = renderer.breakAtEndOfLine( String::length, 10, new String[]{
                "beneeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
                "hat",
                "ziiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiemlich",
                "laaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaange",
                "Wöööööööööööööööööööööööööööööööööööööööööööööööööööörter"
        });

        assertEquals(5, lines.size());

    }

    @Test
    void breaksMultipleWords(){
        String text = "Heute ist ein guter Tag.";
        List<String> lines = renderer.breakAtEndOfLine( String::length, 10, text.split(" "));

        assertEquals(3, lines.size());
        assertEquals("Heute ist", lines.get(0));
        assertEquals("ein guter", lines.get(1));
        assertEquals("Tag.", lines.get(2));
    }
}
