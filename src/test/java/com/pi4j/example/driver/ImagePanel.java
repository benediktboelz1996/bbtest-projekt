package com.pi4j.example.driver;

import java.awt.*;

import javax.swing.JComponent;

public class ImagePanel extends JComponent {
    private Image image;

    public ImagePanel(Image image) {
        this.image = image;
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0,0,100,100);
        g.drawImage(image, 0, 0, null);
    }
}