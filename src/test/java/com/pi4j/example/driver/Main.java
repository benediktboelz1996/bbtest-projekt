package com.pi4j.example.driver;


import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class Main {
    public Main() {
            var bufferedImage = new BufferedImage(128,64,BufferedImage.TYPE_4BYTE_ABGR);
            var graphics = bufferedImage.getGraphics();

            bufferedImage.setRGB(0,60,0xFFFF00);

            JFrame janela = new JFrame("Janela com Background");
            janela.setSize(800, 600);
            janela.setContentPane(new ImagePanel(bufferedImage));
            janela.setVisible(true);

    }

    public static void main(String[] args) {
        new Main();
    }
}