package com.pi4j.example.driver;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Sh1106DisplaySwingImpl implements Sh1106Display {
    private final JFrame frame;
    private final ScreenPanel content;
    private final Map<JButton, Runnable> buttons;
    private static final int WIDTH = 128; // pixel
    private static final int HEIGHT = 64; // pixel

    public Sh1106DisplaySwingImpl() {
        frame = new JFrame("Sh1106 Display Simulation");
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
//        frame.setResizable(false);

        content = new ScreenPanel();
        content.setPreferredSize(new Dimension(WIDTH + 2, HEIGHT + 2));
        content.setImage(new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_BYTE_BINARY));

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(3, 2));
        buttons = new HashMap<>();

        for(int i = 1; i <= 6; i++) {
            JButton button = new JButton(Integer.toString(i));
            Runnable action  = () -> System.out.println("Keine Funktion für Button " + button.getText() + " hinterlegt");
            buttons.put(button, action);
            button.addActionListener(e -> buttons.get(button).run());
            buttonPanel.add(button);
        }

        frame.add(content);
        frame.add(buttonPanel);
        frame.pack();

        frame.setVisible(true);
    }

    @Override
    public void setPixel(int x, int y, boolean on) throws IllegalArgumentException {
        if(x >= 0 && x < getLcdWidth() && y >= 0 && y < getLcdHeight()) {
            content.getImage().setRGB(x, y, on ? Color.BLACK.getRGB() : Color.WHITE.getRGB());
        } else {
            System.out.printf("Pixel nicht auf Screen (width=%d; height=%d): x=%d; y=%d;", getLcdWidth(), getLcdHeight(), x, y);
            System.out.println();
        }
    }

    @Override
    public void render() {
        content.repaint();
    }

    @Override
    public int getLcdWidth() {
        return WIDTH;
    }

    @Override
    public int getLcdHeight() {
        return HEIGHT;
    }

    @Override
    public void clearPixels() {
        content.setImage(new BufferedImage(this.getLcdWidth(), this.getLcdHeight(), BufferedImage.TYPE_BYTE_BINARY));
        content.repaint();
    }

    @Override
    public void setText(String text) {
        var textAsImage = new DisplayTextRenderer().getRenderedTextImage(text, getLcdWidth(), getLcdHeight());
        new BitmapHelper().setImageData(this, textAsImage,0,0);
    }

    @Override
    public void setText(String text, TextRenderOptions options) {
        options.setDisplayHeight(getLcdHeight());
        options.setDisplayWidth(getLcdWidth());
        var textAsImage = new DisplayTextRenderer().getRenderedTextImage(text, options);
        new BitmapHelper().setImageData(this, textAsImage,0,0);
    }

    public void setAction(int buttonIndex, Runnable action) {
        Optional<JButton> buttonAtIndex = buttons.keySet().stream().filter(button -> button.getText().equals(Integer.toString(buttonIndex))).findFirst();
        if(buttonAtIndex.isPresent()) {
            buttons.put(buttonAtIndex.get(), action);
        } else {
            System.out.println("Kein Button " + buttonAtIndex + " vorhanden");
        }
    }

    @Override
    public void close() throws IOException {

    }

    private static class ScreenPanel extends JPanel {
        private BufferedImage image;

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            g.setColor(Color.BLUE);
            g.drawRect(0, 0, image.getWidth() + 1, image.getHeight() + 1);
            g.drawImage(image, 1, 1, this);
        }

        public BufferedImage getImage() {
            return image;
        }

        public void setImage(BufferedImage image) {
            this.image = image;
        }
    }
}
