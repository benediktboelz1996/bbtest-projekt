package com.pi4j.example.driver;

import java.awt.*;
import java.awt.image.BufferedImage;

public class SwingDisplayTest {
    private static BitmapHelper bitmapHelper = new BitmapHelper();
    private static Sh1106Display display = new Sh1106DisplaySwingImpl();

    public static void main(String[] args){
        display.setText("⚽Team 1\n1 : 2\nTeam 2");
        display.render();
        //writeText("Test");

        ((Sh1106DisplaySwingImpl)display).setAction(1, () -> {
            TextRenderOptions options = TextRenderOptions.builder()
                    .build();
            options.setFontSize(60);
            options.setStartY(-30);
            display.setText("1 : 3", options);
            display.render();
        });
        ((Sh1106DisplaySwingImpl)display).setAction(2, () -> {
            writeText("hmmp ❤️");
        });
        ((Sh1106DisplaySwingImpl)display).setAction(3, () -> {
            writeText("holla yay😂");
        });
        ((Sh1106DisplaySwingImpl)display).setAction(4, () -> {
            writeText("holla yay😘😘");
        });
    }

    private static void writeText(String text) {
        BufferedImage image = new BufferedImage(display.getLcdWidth(), display.getLcdHeight(), BufferedImage.TYPE_4BYTE_ABGR);
        var graphics = image.createGraphics();
        graphics.setColor(Color.WHITE);

        int fontSize = 30;
        var font = new Font ("FreeSansBold", Font.PLAIN, fontSize);
        graphics.setFont(font);
        graphics.drawString(text, 0,  + fontSize);

        graphics.dispose();
        image.flush();



        display.clearPixels();
        bitmapHelper.setImageData(display, image, 0, 0);
        display.render();
    }
}
