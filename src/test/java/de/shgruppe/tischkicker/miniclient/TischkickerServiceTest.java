package de.shgruppe.tischkicker.miniclient;

import com.google.gson.Gson;
import tischkicker.models.Spiel;

import java.util.List;

/**
 * Benötigt einen vorher laufenden Server. Je nachdem was man testen will, sollte auch ein Spiel gestartet werden
 */
public class TischkickerServiceTest {
    public static void main(String[] args) {
        TischkickerService tischkickerService = new TischKickerServiceImpl();
        Gson gson = new Gson();

        List<Spiel> playableGames = tischkickerService.getSpielbareSpiele();
        System.out.println(gson.toJson(playableGames));

//        if(playableGames.size() > 0) {
//            tischkickerService.starteSpiel(playableGames.get(0).getSpielID());
//        }

        tischkickerService.setSpielErgebnisCallback(spielErgebnis -> System.out.println("TOOOOOR!!! Vielleicht auch Spielstandreduktion?! " + gson.toJson(spielErgebnis)));
        tischkickerService.setSpielBeendetCallback(spielBeendetMessage -> System.out.println("Aus und vorbei, das Spiel ist aus und vorbei! " + gson.toJson(spielBeendetMessage)));

//        tischkickerService.spielstandInkrementieren(1);
//        tischkickerService.spielstandDekrementieren(2);
//        tischkickerService.seitenwechsel();

        while (true) {
            // Einfach laufen lassen bis abgeschossen wird, um Callbacks zu testen
        }
    }
}
